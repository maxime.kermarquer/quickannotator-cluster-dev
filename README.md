

*  QuickAnnotator usage on ICM cluster


### Start QuickAnnotator WebUI


**1.** Connect to cluster login node 

```bash
ssh login02
```

**2.** Allocate a GPU node

```bash
salloc -p gpu -t 02:00:00 --mem=16G -c 4 --gres=gpu:1 -J QuickAnnotator
```

**3.** Connect to GPU node allocated

```bash
ssh $SLURM_NODELIST 
```

**4.**  Load python environment for QuickAnnotator

```bash
module load QuickAnnotator
```

**5.** Start QuickAnnotator WebUI
```bash
run_QuickAnnotator.sh
```

The output will indicate the listening port to use

**6.** From an another terminal initiate the ssh tunnel the indicated port 

```bash
ssh -L <port>:localhost:<port> login02 
ssh -L <port>:localhost:<port> lmgpu01
```

**7.** Open a Web Brower and copy / paste the URL indicated by QuickAnnotator

```bash
http://0.0.0.0:<port>
```

#### Examples

* For the steps **1 to 5**

![](img/quickannotator-1.png) 
![](img/quickannotator-2.png) 

* For the step **6**

![](img/quickannotator-3.png) 

* For the step **7**

![](img/quickannotator-4.png) 
