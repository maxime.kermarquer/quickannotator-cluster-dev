#!/bin/bash

###################
#### GPU check ####
###################

if [ ! -z ${SLURM_JOB_ID} ]; then
    ALLOC_GPU_ID=$(scontrol show job --details ${SLURM_JOB_ID}  |grep -e "GRES=gpu" | sed 's/.*IDX://;s/)//')
    echo -e "\e[33mAllocated GPU : ${ALLOC_GPU_ID}\e[0m"
else
    echo -e "\e[1;31mPlease allocate a gpu node\e[0m"
    echo -e "\e[33msalloc -p gpu -t 02:00:00 --mem=16G -c 4 --gres=gpu:1 -J "QuickAnnotate"\e[0m"
    exit 1
fi

nvidia-smi >> /dev/null  2> /dev/null
RC_NVIDIA=${?}

if [ ${RC_NVIDIA} -ne 0 ]; then
    echo -e "\e[1;31mPlease use QuickAnnotator on a gpu node\e[0m"
    exit 1
fi

##############################
#### Listening port check ####
##############################

RC_NETSTAT=0
PORT=$((5555+${UID}))

while [ ${RC_NETSTAT} -eq 0 ]; do
    echo -e "\e[34mTest of port ${PORT} ...\e[0m"
    netstat -tulpn | grep LISTEN |grep "${PORT}" &> /dev/null
    RC_NETSTAT=${?}
    if [ ${RC_NETSTAT} -eq 0 ]; then
        echo -e "Listening port ${PORT} in usage."
        if [ ${PORT} -eq 5575 ]; then
            echo -e "\e[1;31mAll range of Listening ports 5555-5575 in usage.\e[0m"
            exit 1
        fi
        ((PORT++))
    else
        echo -e "\e[32mListening port ${PORT} is free.\e[0m"
        break;
    fi
done

echo -e "\e[33mListening port to use : ${PORT}\e[0m"

#####################################
#### Get QuickAnnotator sources  ####
#####################################

if [ ! -f ${HOME}/QuickAnnotator/QA.py ]; then
    git clone https://github.com/choosehappy/QuickAnnotator.git ${HOME}/QuickAnnotator
fi

#############################################
#### Update QuickAnnotator configuration ####
#############################################

# GPU id
sed -i "s/^gpuid =.*/gpuid = ${ALLOC_GPU_ID}/g" $HOME/QuickAnnotator/config/config.ini

# Port
sed -i "s/^port =.*/port = ${PORT}/g" $HOME/QuickAnnotator/config/config.ini


#############################
#### Run QuickAnnnotator ####
#############################
cd ${HOME}/QuickAnnotator
python QA.py&

if [ ${?} -ne 0 ]; then
    echo -e "\e[1;31mError at QuickAnnotator start.\e[0m"
    exit 1
fi

PID_QA=${!}


#######################################################
#### Display commands to connect to QuickAnnotator ####
#######################################################
sleep 15

echo -e "\e[1m**************************************************************************\e[0m"
echo -e "\e[1m**************************************************************************\e[0m\n"

echo -e "* To connect to QuickAnnotator :"
echo -e "\e[1m1. Open a new terminal and run these commands :\e[0m\n"

echo -e "\e[34m\tssh -L ${PORT}:localhost:${PORT} login02 \e[0m"
echo -e "\e[34m\tssh -L ${PORT}:localhost:${PORT} $(hostname)\e[0m\n"

echo -e "\e[1m2. In your web browser copy / paste the following url :\e[0m\n"
echo -e "\e[34m\thttp://0.0.0.0:${PORT}/\e[0m\n"

echo -e "\e[1m**************************************************************************\e[0m"
echo -e "\e[1m**************************************************************************\e[0m\n"

echo -e "\e[1mCtrl + C to stop QuickAnnotator\e[0m\n"


function stop_quickannotator()
{
    kill ${PID_QA}
    exit 0
}

trap stop_quickannotator SIGINT SIGTERM SIGQUIT

while true
do
    sleep 10
done

exit 0

